# variables.tf

variable "name" {
  description = "The Name of the SNS topic to create"
  type        = string
}

variable "fifo_queue" {
  description = "Is this a FIFO queue"
  type        = bool
  default     = true
}

variable "max_message_size" {
  description = "Is this a FIFO queue"
  type        = number
  default     = 2048
}


variable "visibility_timeout_seconds" {
  description = "Is this a visibility_timeout_seconds queue"
  type        = number
  default     = 86400
}
 
variable "message_retention_seconds" {
  description = "Is this a message_retention_seconds queue"
  type        = number
  default     = 86400
}
variable "content_based_deduplication" {
  description = "Is this a FIFO queue"
  type        = bool
  default     = true
}

variable "tags" {
  description = "Tags to be deployed"
  type        = map(string)
  default     = {}
}

variable "display_name" {
  description = "Display name for the SQS Queue"
  type        = string
  default     = null
}