# main.tf

resource "aws_sqs_queue" "my_sqs_queue" {
  name                        = var.name
  tags                        = var.tags
  fifo_queue                  = true
  content_based_deduplication = true
  kms_master_key_id                 = "alias/aws/sqs"
  kms_data_key_reuse_period_seconds = 300
  max_message_size          = var.max_message_size
  message_retention_seconds = var.message_retention_seconds
}
