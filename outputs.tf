# outputs.tf

output "sqs_queue_id" {
  description = "ID of SQS Queue"
  value       = aws_sqs_queue.my_sqs_queue.id
}

output "sqs_queue_arn" {
  description = "ARN of SQS Queue"
  value       = aws_sqs_queue.my_sqs_queue.arn
}


